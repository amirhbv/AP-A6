#include "card.hpp"

Archer::Archer()
{
	strength = archerStrength;
	agility = archerAgility;
	intelligence = archerIntelligence;
}

Dragon::Dragon()
{
	strength = dragonStrength;
	agility = dragonAgility;
	intelligence = dragonIntelligence;
}

Giant::Giant()
{
	strength = giantStrength;
	agility = giantAgility;
	intelligence = giantIntelligence;
}

Witch::Witch()
{
	strength = witchStrength;
	agility = witchAgility;
	intelligence = witchIntelligence;
}

Goblin::Goblin()
{
	strength = goblinStrength;
	agility = goblinAgility;
	intelligence = goblinIntelligence;
}

Ghost::Ghost()
{
	strength = ghostStrength;
	agility = ghostAgility;
	intelligence = ghostIntelligence;
}

bool Strength::fight(Strength* opponent)
{
	if (intelligence + agility >= opponent->intelligence + opponent->agility)
	{
		intelligence += 10;
		check(intelligence);
		return WIN;
	}
	else
	{
		opponent->intelligence += 10;
		check(opponent->intelligence);
		return LOSE;
	}
}

bool Strength::fight(Intelligence* opponent)
{
	if (agility >= opponent->agility)
	{
		strength += 20;
		check(strength);
		return WIN;
	}
	else
	{
		opponent->strength += 20;
		check(opponent->strength);
		return LOSE;
	}
}

bool Strength::fight(Agility* opponent)
{
	agility += 10;
	check(agility);
	return WIN;
}


bool Agility::fight(Strength* opponent)
{
	opponent->agility += 10;
	check(opponent->agility);
	return LOSE;
}

bool Agility::fight(Agility* opponent)
{
	if (intelligence + strength >= opponent->intelligence + opponent->strength)
	{
		intelligence += 5;
		strength += 5;
		check(intelligence);
		check(strength);
		return WIN;
	}
	else
	{
		opponent->intelligence += 5;
		opponent->strength += 5;
		check(opponent->intelligence);
		check(opponent->strength);
		return LOSE;
	}
}

bool Agility::fight(Intelligence* opponent)
{
	if (strength >= opponent->strength)
	{
		intelligence += 10;
		agility += 10;
		check(intelligence);
		check(agility);
		return WIN;
	}
	else
	{
		opponent->intelligence += 10;
		opponent->agility += 10;
		check(opponent->intelligence);
		check(opponent->agility);
		return LOSE;
	}
}

bool Intelligence::fight(Strength* opponent)
{
	return !opponent->fight(this);
}

bool Intelligence::fight(Agility* opponent)
{
	return !opponent->fight(this);
}

bool Intelligence::fight(Intelligence* opponent)
{
	bool win = 0;
	if (intelligence - opponent->intelligence >= 20 ||
	        opponent->intelligence - intelligence >= 20)
	{
		if(agility > opponent->agility)
			win = 1;
	}
	else if (strength + agility >= opponent->strength + opponent->agility)
		win = 1;
	if(win)
	{
		strength += 5;
		agility += 5;
		check(strength);
		check(agility);
		return WIN;
	}
	else
	{
		opponent->strength += 5;
		opponent->agility += 5;
		check(opponent->strength);
		check(opponent->agility);
		return LOSE;
	}
}

void Card::check(int &x)
{
	if (x > 100)
		x = 100;
}
