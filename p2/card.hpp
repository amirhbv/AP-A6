#ifndef CARD_H
#define CARD_H

#include "gameData.hpp"

#define WIN 1
#define LOSE 0

class Strength;
class Agility;
class Intelligence;

class Card
{
public:
	virtual bool fight(Card* card) = 0;
	virtual bool fight(Strength*) = 0;
	virtual bool fight(Agility*) = 0;
	virtual bool fight(Intelligence*) = 0;
	int strength, agility, intelligence;
	void check(int &x);
};


class Strength : public Card
{
public:
	bool fight(Card* card) { return !card->fight(this); };
	bool fight(Strength*);
	bool fight(Agility*);
	bool fight(Intelligence*);

};

class Agility : public Card
{
public:
	bool fight(Card* card) { return !card->fight(this); };
	bool fight(Strength*);
	bool fight(Agility*);
	bool fight(Intelligence*);
};

class Intelligence : public Card
{
public:
	bool fight(Card* card) { return !card->fight(this); };
	bool fight(Strength*);
	bool fight(Agility*);
	bool fight(Intelligence*);
};

class Archer : public Agility
{
public:
	Archer();
};

class Goblin : public Agility
{
public:
	Goblin();
};

class Witch : public Intelligence
{
public:
	Witch();
};

class Ghost : public Intelligence
{
public:
	Ghost();
};

class Dragon : public Strength
{
public:
	Dragon();
};

class Giant : public Strength
{
public:
	Giant();
};

#endif