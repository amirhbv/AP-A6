#include "player.hpp"

using namespace std;

#include <iostream>
void Player::addCard(string _cardName)
{
	if(_cardName == cardNames[ARCHER])
		cards.push_back(new Archer());
	else if(_cardName == cardNames[GIANT])
		cards.push_back(new Giant());
	else if(_cardName == cardNames[WITCH])
		cards.push_back(new Witch());
	else if(_cardName == cardNames[DRAGON])
		cards.push_back(new Dragon());
	else if(_cardName == cardNames[GOBLIN])
		cards.push_back(new Goblin());
	else if(_cardName == cardNames[GHOST])
		cards.push_back(new Ghost());
}
