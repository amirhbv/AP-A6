#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <string>
#include <vector>

#include "player.hpp"

#define ONE 1
#define	TWO 2
#define FOUR 4
#define FIRST 0
#define SECOND 1

class Game {
public:
	Game();
	void add_player_card(int playerNumber, std::string cardName);
	void play();
private:
	std::vector<Player*> players;
};

#endif
