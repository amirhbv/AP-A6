#include "game.hpp"

using namespace std;

Game::Game()
{
	players.push_back(new Player());
	players.push_back(new Player());
}
void Game::add_player_card(int playerNumber, string cardName)
{
	players[playerNumber - 1]->addCard(cardName);
}

void Game::play()
{
	int firstPlayerCardIndex, secondPlayerCardIndex;
	firstPlayerCardIndex = secondPlayerCardIndex = 0;

	while(firstPlayerCardIndex < FOUR && secondPlayerCardIndex < FOUR)
	{
		if ( players[FIRST]->cards[firstPlayerCardIndex]->fight(players[SECOND]->cards[secondPlayerCardIndex]) == WIN )
			secondPlayerCardIndex++;
		else
			firstPlayerCardIndex++;
	}
	if(secondPlayerCardIndex == FOUR)
		cout << ONE << endl;
	else
		cout << TWO << endl;
}