#ifndef PLAYER_H
#define PLAYER_H

#include <vector>
#include <string>

#include "card.hpp"

enum CardName
{
	ARCHER = 0,
	GIANT,
	WITCH,
	DRAGON,
	GOBLIN,
	GHOST
};
const std::string cardNames[]
{
	"archer",
	"giant",
	"witch",
	"dragon",
	"goblin",
	"ghost"
};

class Player
{
public:
	void addCard(std::string cardName);
	std::vector<Card*> cards;	
};
#endif