const int archerStrength = 35;
const int archerAgility = 75;
const int archerIntelligence = 40;

const int giantStrength = 85;
const int giantAgility = 30;
const int giantIntelligence = 35;

const int dragonStrength = 70;
const int dragonAgility = 50;
const int dragonIntelligence = 30;

const int goblinStrength = 50;
const int goblinAgility = 90;
const int goblinIntelligence = 10;

const int witchStrength = 40;
const int witchAgility = 30;
const int witchIntelligence = 80;

const int ghostStrength = 40;
const int ghostAgility = 40;
const int ghostIntelligence = 40;