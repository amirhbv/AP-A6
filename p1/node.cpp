#include "node.hpp"

using namespace std;

void Operator::addOperand(Node* newNode)
{
	if(!isOperandOk())
		throw NumberOfOperandException(TOO_MANY_ARGUMENTS);
	operands.push_back(newNode);
}

int AddOperator::calculate()
{
	if(operands.size() < 2)
		throw NumberOfOperandException(TOO_FEW_ARGUMENTS);
	return operands[0]->calculate() + operands[1]->calculate();
}

bool AddOperator::isOperandOk()
{
	return (operands.size() + 1 <= 2);
}

int MultipyOperator::calculate()
{
	if(operands.size() < 2)
		throw NumberOfOperandException(TOO_FEW_ARGUMENTS);
	return operands[0]->calculate() * operands[1]->calculate();
}


bool MultipyOperator::isOperandOk()
{
	return (operands.size() + 1 <= 2);
}

int NotOperator::calculate()
{
	if(operands.size() < 1)
		throw NumberOfOperandException(TOO_FEW_ARGUMENTS);
	return (-1) * operands[0]->calculate();
}

bool NotOperator::isOperandOk()
{
	return (operands.size() + 1 <= 1);
}

int MedianOperator::calculate()
{
	vector<int> num;
	for(unsigned int i  = 0; i < operands.size(); i++)
		num.push_back(operands[i]->calculate());

	sort(num.begin(), num.end());

	return (num[(operands.size() - 1) /2] + num[operands.size() /2]) / 2;
}

bool MedianOperator::isOperandOk()
{
	return (TRUE);
}

int Leaf::calculate()
{
	return value;
}