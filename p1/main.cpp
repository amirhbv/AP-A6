#include <iostream>

#include "tree.hpp"

using namespace std;

int main()
{
	Tree tree;
	tree.add_operator_node(0, NO_PARENT, Add);
	tree.add_number_node(1, 0, 1);
	tree.add_number_node(2, 0, 2);
	// tree.add_number_node(3, 0, 3);
	// tree.add_number_node(4, 0, 4);
	// tree.add_number_node(5, 0, 5);
	int result = tree.evaluate();
	cout << result << endl;
	return 0;
}
