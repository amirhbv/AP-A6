#include <vector>

#include "node.hpp"
#include "exceptions.hpp"

#define NO_PARENT -1

enum OperatorType { Add, Multiply, Not, Median};

class Tree {
public:
	void add_number_node(int index, int parentIndex, int value);
	void add_operator_node(int index, int parentIndex, OperatorType type);
	int evaluate();
private:
	std::vector<Node*> nodes;

	Operator* findNode(int index);
};
