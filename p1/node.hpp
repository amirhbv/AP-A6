#ifndef NODE_H
#define NODE_H

#include <vector>
#include <algorithm>

#include "exceptions.hpp"

#define TRUE 1

class Node
{
public:
	Node(int _index) : index(_index){};
	virtual int calculate() = 0;
	int index;
};

class Operator : public Node
{
public:
	Operator(int _index) : Node(_index){};
	virtual void addOperand(Node*);
protected:
	std::vector<Node*> operands;
	virtual bool isOperandOk() = 0;
};

class AddOperator : public Operator
{
public:
	AddOperator(int _index) : Operator(_index){};
	int calculate();
private:
	bool isOperandOk();
};

class NotOperator : public Operator
{
public:
	NotOperator(int _index) : Operator(_index){};
	int calculate();
private:
	bool isOperandOk();
};

class MultipyOperator : public Operator
{
public:
	MultipyOperator(int _index) : Operator(_index){};
	int calculate();
private:
	bool isOperandOk();
};

class MedianOperator : public Operator
{
public:
	MedianOperator(int _index) : Operator(_index){};
	int calculate();
private:
	bool isOperandOk();
};

class Leaf : public Node
{
public:
	Leaf(int _index, int _value) : Node(_index), value(_value){};
	int calculate();
private:
	int value;
};

#endif