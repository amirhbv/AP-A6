#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>

#define TOO_FEW_ARGUMENTS 1
#define TOO_MANY_ARGUMENTS 2

class Exception
{
public:
	virtual std::string what() {return "";};
};
class NumberOfOperandException: public Exception
{
public:
	NumberOfOperandException(int _type) : type(_type) {};
	std::string what()
	{
		if (type == TOO_MANY_ARGUMENTS)
			return "Operator has to many argumants!";
		else if (type == TOO_FEW_ARGUMENTS)
			return "Operator has to few argumants to operate!";
		else
			return "Operator conditions not met!";
	}
private:
	int type;
};

class ParentIndexNotAvailableException: public Exception
{
public:
	std::string what()
	{
		return "Parent node is not available!";
	}
};

class BadParentNodeException: public Exception
{
public:
	std::string what()
	{
		return "Parent node is not an operator!";
	}
};

class InvalidOperatorType: public Exception
{
public:
	std::string what()
	{
		return "Operator type is invalid!";
	}
};

#endif
