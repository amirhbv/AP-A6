#include <iostream>
#include "tree.hpp"

using namespace std;

void Tree::add_number_node(int index, int parentIndex, int value)
{
	try
	{
		Operator *parentNode = findNode(parentIndex);
		Leaf *newLeaf = new Leaf(index, value);
		parentNode->addOperand(newLeaf);
		nodes.push_back(newLeaf);
	}
	catch (Exception &e)
	{
		cerr << e.what() << endl;
	}
	catch(...){}

}

void Tree::add_operator_node(int index, int parentIndex, OperatorType type)
{
	try
	{
		Operator *parentNode, *newNode;
		switch (type)
		{
		case Add:
			newNode = new AddOperator(index);
			break;
		case Multiply:
			newNode = new MultipyOperator(index);
			break;
		case Not:
			newNode = new NotOperator(index);
			break;
		case Median:
			newNode = new MedianOperator(index);
			break;
		default:
			throw InvalidOperatorType();
		}
		if (parentIndex == NO_PARENT)
		{
			nodes.push_back(newNode);
			return;
		}
		parentNode = findNode(parentIndex);
		parentNode->addOperand(newNode);
		nodes.push_back(newNode);
	}
	catch (Exception &e)
	{
		cerr << e.what() << endl;
	}
}

int Tree::evaluate()
{
	int result = 0;
	try
	{
		result = nodes[0]->calculate();
	}
	catch (Exception &e)
	{
		cerr << e.what() << endl;
	}
	return result;
}

Operator* Tree::findNode(int index)
{
	for (unsigned int i = 0; i < nodes.size(); i++)
		if (nodes[i]->index == index)
		{
			Operator* newOperator = dynamic_cast<Operator*>(nodes[i]);
			if (newOperator)
				return newOperator;
			else
				throw BadParentNodeException();
		}
	throw ParentIndexNotAvailableException();
}